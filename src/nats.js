const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions().setDeliverAllAvailable();
    const sub = conn.subscribe('users', opts);

    const db = mongoClient.db('mockDb');
    const coll = db.collection('mockColl');
    
    sub.on('message', (msg) => {
      //sub.publish('users', msg.getData);
      console.log('Received message [' + msg.getSequence() + '] = ' + msg.getData() + ';');
    });
  
  });

  return conn;
};
