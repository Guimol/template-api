const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {

  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  const db = mongoClient.db('mockDb'); //pegar um database do cliente mongo
  const coll = db.collection('mockColl'); //pegar uma colecao em especifico do database

  api.get("/", (req, res) => res.status(200).json("Hello, World!"));

  api.post("/users", async (req, res) => { //async para funcao findOne

    const user = req.body; //corpo da requisicao
    const keys = Object.keys(user); //valores das chaves de usuario
    let flag = 1 //flag das verificacoes 1 = certo, -1 = erro

    const re_nome = /^[a-zA-Z]+$/; //regex (regular expression) compara se nome somente alfabeto
    const re_email = /\S+@\S+\.\S+/; //regex (regular expression) compara se email tem "cara de email"
    const re_pass = /^[a-z0-9]+$/i; //regex (regular expression) compara se senha eh alfanumerico
   
    //erros
    for(let i = 0; i < keys.length; i++){
      if(user[keys[i]] == '' || user[keys[i]].length <= 0){ //teste de campo vazio ou tamanho zero
        //erro 400 (BAD REQUEST)
        res.status(400).json({"error": "Request body had missing field {" + keys[i] + "}\","});
        flag = -1;
      }
    }

    if(flag === 1 && !(re_nome.test(user.name))){ //teste de nome segue a regex
      //erro 400 (BAD REQUEST)
      res.status(400).json({"error": "Request body had malformed field {name}\","});
      flag = -1;
    }

    if(flag === 1 && !(re_email.test(user.email)) && !(await coll.findOne({email: user.email}))){ //teste de email segue a regex e se email eh unico
      //erro 400 (BAD REQUEST)
      res.status(400).json({"error": "Request body had malformed field {email}\","});
      flag = -1;
    }

    if(flag === 1 && user.password.length < 8 || user.password.length > 32 && !(re_pass.test(user.password))){ //teste de senha segue a regex e se senha tem tamanho entre 8 e 32
      //erro 400 (BAD REQUEST)
      res.status(400).json({"error": "Request body had malformed field {password}\","});
      flag = -1;
    }
    
    if(flag === 1 && user.password !== user.passwordConfirmation){ //teste se senha e confirmacao de senha sao iguais
      //erro 422 (UNPROCESSABLE ENTITY)
      res.status(422).json({"error": "Password confirmation did not match\","});
      flag = -1;
    }
  
    if(flag === 1){ //passou em todos os testes de verificacao
      //deu certo
      let id = uuid(); //geracao automatica de id
      let newUser = {"user": {"id": id, "name": user.name, "email": user.email}}
      res.status(201).json(newUser);
      
      let userCreated = {"eventType": "UserCreated", "entityID": id, "entityAggregate": newUser}; //formato da mensagem a ser enviada

      if(!(await coll.findOne({id: id}))){ //busca se o usuario eh um usuario novo
        stanConn.publish('users', userCreated); //publica a mensagem no broker
      }
    }
  });

  api.delete("/users/:uuid", async (req,res) =>{ //async para funcao findOne
    let id = req.params.uuid; //parametros enviados pelo req
    let id_decriptado;

    let flag = 1; //flag das verificacoes 1 = certo, -1 = erro
    
    if(!(req.get("Authentication")) || !(req.get("Authentication").includes('Bearer', 0)) || req.get("Authentication").split(' ')[1] === ""){ //verifica se o cabecalho existe no formato "Authentication": Bearer <token>
      //erro 401 (UNAUTHORIZED)
      res.status(401).json({"error": "Acess Token not found\","});
      flag = -1;
    }

    if(flag === 1){ //caso passe da verificacao de cabecalho pega o token e o decripta
      let key = req.get("Authentication").split(' ')[1]; //pega o token
      id_decriptado = jwt.verify(key, secret).id //decriptacao
    }

    if(flag === 1 && id !== id_decriptado){ //erro de validacao
      //erro 403 (FORBIDDEN) 
      res.status(403).json({"error": "Acess Token did not match User ID\","});
      flag = -1;
    }
    
    if(flag === 1){ //passou em todos os testes de verificacao
      //deu certo
      let userDeleted = {"eventType": "UserDeleted", "entityID": id, "entityAggregate": {}}; //formato da mensagem a ser enviada
      res.status(200).json({"id": id});

      if(await coll.findOne({id: id})){ //verifica se existe um usuario a ser deletado
        stanConn.publish('users', userDeleted); //publica a mensagem no broker
      }
    }
  });
  
  return api;
};
